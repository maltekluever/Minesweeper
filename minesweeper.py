from tkinter import *
from PIL import *
from random import *
from colour import Color
import math as math


# configure window
mw = Tk()                           # initialize window
mw.resizable(False, False)          # make window not resizable
mw.title("Minesweeper")             # set window title

# open window in cemter of screen
ws = mw.winfo_screenwidth()
hs = mw.winfo_screenheight() 
x = (ws/2)
y = (hs/2)
mw.geometry('+%d+%d' % (x,y))

# config game
numberofbombs = 10
colors = ('#99db99','#dbff8f','#ffef8f','#ffd48f','#ffbe8f','#ffb48f','#ffa18f','#ff948f','#ff6565','#ff4c4c')

# define images
uncovered = PhotoImage(file="covert.png")
bomb = PhotoImage(file="bomb.png")
imggameover = PhotoImage(file="gameover.png")
mw.imggameover = imggameover
imgvictory = PhotoImage(file="victory.png")
mw.victory = imgvictory
for i in range(0,9):
    imgrevealed = PhotoImage(file="revealed"+str(i)+".png")
    setattr(mw,'revealed'+str(i),imgrevealed)

# show game-over-screen
def gameover():
    global imggameover
    gameover = Label(mw, image=imggameover, bd=0)
    gameover.place(x=0, y=0, relwidth=1, relheight=1)    

# show victory-screen
def victory():
    global victory
    victory = Label(mw, image=imgvictory, bd=0)
    victory.place(x=0, y=0, relwidth=1, relheight=1)

# uncover a field and check for a mine (left-click-event)
def uncover(x,y):
    global df
    if(df[str(x)+','+str(y)]['mine']==0):    
        df[str(x)+','+str(y)]['state'] = 'revealed'
        nbombs=countneighbourbombs(x,y)
        df[str(x) + ',' + str(y)]['Button'].config(image=getattr(mw,'revealed'+str(nbombs)),bg=str(colors[int(nbombs)]))
    else:
        gameover()

# check how many bombs are in the neighbouring fields
def countneighbourbombs(x,y):
    global df
    mine=0
    for xx in range(max(0,x-1),min(9,x+1)+1):
        for yy in range(max(0,y-1),min(9,y+1)+1):
            if(df[str(xx)+','+str(yy)]['mine']==1):
                mine += 1
    if(mine==0):
        for xx in range(max(0,x-1),min(9,x+1)+1):
            for yy in range(max(0,y-1),min(9,y+1)+1):
                if(xx!=x or yy!=y):
                    if(df[str(xx)+','+str(yy)]['state']!='revealed'):
                        uncover(xx,yy)
    return str(mine)
   
# flag a field as a bomb (right-click-event)        
def flag(x,y):
    global df, bomb
    if(df[str(x)+','+str(y)]['state']=='covert'):
        df[str(x)+','+str(y)]['state']='flagged'
        df[str(x)+','+str(y)]['Button'].config(image=bomb,bg="grey")
        if countflaggedbombs()==numberofbombs:
            victory()
    elif(df[str(x)+','+str(y)]['state']=='flagged'):
        df[str(x)+','+str(y)]['state']='covert'
        df[str(x)+','+str(y)]['Button'].config(image=uncovered,bg="lightsteelblue")

# count the number of correctly flagged bombs
def countflaggedbombs():
    global df
    bombs = 0
    for x in range(0,10):
        for y in range(0,10):    
            if(df[str(x)+','+str(y)]['state']=='flagged' and df[str(x)+','+str(y)]['mine']==1):
                bombs+=1
    return bombs

# make data frame 
df = {}
for x in range(0,10):
    for y in range(0,10):
       df[str(x)+','+str(y)] = {'Button':Button(mw),'state':'covert','mine':0}
       df[str(x)+','+str(y)]['Button'].config(image=uncovered,width="50",height="50",activebackground="black",bg="lightsteelblue",compound="center", bd=1)
       df[str(x)+','+str(y)]['Button'].grid(row=y,column=x) 
       df[str(x)+','+str(y)]['Button'].bind('<Button-1>',lambda event, x=x, y=y: uncover(x,y))             # define left-click-event
       df[str(x)+','+str(y)]['Button'].bind('<Button-3>',lambda event, x=x, y=y: flag(x,y))                # define right-click event
       
# distribute 25 mines randomly across the field
test = sample(range(0, 99), numberofbombs)
for i in test:
    df[str(math.floor(i/10))+','+str(i%10)]['mine'] = 1; 

mw.mainloop()